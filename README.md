# README - Dating App #

A Dating app where the user will be able to : 

* Create and customize a dating profile
* Interact with other users profile ( like, send message, view profile)


### How do I get set up? ###

#### 1 - Create a cloudinary account ####

This app hosts its images on https://cloudinary.com/. 
Once registered

* Navigate to server/API
* Create a file called "appsettings.json"
* Copy the following information and update the CloudinarySettings with your cloudinary account information.

```
{
    "Logging": {
        "LogLevel": {
            "Default": "Information",
            "Microsoft": "Information",
            "Microsoft.Hosting.Lifetime": "Information"
        }
    },
    "CloudinarySettings": {
        "CloudName": "",
        "ApiKey": "",
        "ApiSecret": ""
    },
    "AllowedHosts": "*"
}
```

#### 2 - Launch the server ####

* Navigate to server/API
* Run the command 'dotnet build' to install the dependencies 
* Run the commant 'dotnet run' to launch the server

#### 3 - Launch the UI ####

* Navigate to client/src
* Run the command 'npm install' to install the dependencies 
* Run the command 'npm start' to launch the UI
* In a browser, navigate to https://localhost:4200


That should get you going ! 


